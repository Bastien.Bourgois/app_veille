<head>
  <meta charset="utf-8">
  <?php echo "<title>" . $title . "</title>"; ?>
  <link rel="stylesheet" type="text/css" href="template/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="template/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="stylesheet" type="text/css" href="font-awesome.min.css">
  <script src="/template/script/random.js" type="text/javascript"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
