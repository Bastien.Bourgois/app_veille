<?php
    include 'core/session.php';
    include 'core/loged.php';
    include 'core/database.php';
?>
<!doctype html>
<html>
<head>
   <title>Page d'accueil</title>
   <meta charset>
   <?php
     include ('include/style.php')
    ?>
   <style media="screen">
	   body {
		   background-image: url(tree1.jpg);
		   background-position: left;
		   background-repeat: no-repeat;
		   background-attachment: fixed;
	   }
	   img {
		   max-width:60px;
		   height: 60px;
		   border-radius: 2px;
		   margin-bottom: 4px;
		   float: left;
		   margin-right: 15px;
		   background-color: #F0F6C6;
		   padding: 3px;
		   border-radius: 2px;
		   border: 1px #85BEB8 solid;
	   }
   </style>
</head>
<body>

<?php include 'template/header.php'; ?>

   <div class="container margintop">
      <div class="row">
         <div class="col-md-12">
            <div class="hello">
<?php
                  echo "\t\t\t\t<h3>Bonjour <span class='pseudo'>".$username."</span></h3>\n";
                  include 'include/date.php';
                  echo "\t\t\t\tLES VEILLES DU JOUR : <span class='dark'>".$dday." ".$jour." ".$month." ".$annee."</span><br>\n";
?>
            </div>
         </div>
      </div>
      <div class="row">
<?php
		$query="SELECT * FROM ( SELECT * FROM veille ORDER BY date desc ) v GROUP BY v.id_user";
		$result=mysqli_query($handle,$query);

		while($line=mysqli_fetch_array($result)) {
      $id_user=$line['id_user'];
      $id_veille=$line['id'];

      echo "\t\t<div class='col-md-3'>\n";
		echo "\t\t\t<a href='veille.php?id=".$line['id']."'>\n";
      echo "\t\t\t<div id='veille'>\n";

      $query="SELECT * FROM users WHERE id='$id_user'";
      $user_r=mysqli_query($handle,$query);
      $line_user=mysqli_fetch_array($user_r);

      $query="SELECT *, DATE_FORMAT(veille.date, '%d/%m à %H:%i') as heure FROM veille WHERE id='$id_veille'";
      $veille_r=mysqli_query($handle,$query);
      $line_veille=mysqli_fetch_array($veille_r);
			echo "\t\t\t\t<div class='id'>\n";
			echo "\t\t\t\t\t<div class='row'>\n";
			echo "\t\t\t\t\t\t<div class='col-md-4'>\n";
			echo "\t\t\t\t\t\t\t<img class='avatar' src='../uploads/".$line_user["img"]."'>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t\t<div class='col-md-8'>\n";
			echo "\t\t\t\t\t\t\t<h4 class='nom'>".$line_user['firstname']." ".$line_user['name']."</h4>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t</div>\n";
			echo "\t\t\t\t</div>\n";
			echo "\t\t\t\t<div class='subject'>\n";
			echo "\t\t\t\t\t<div class='row'>\n";
			echo "\t\t\t\t\t\t<div class='col-md-3'>\n";
			echo "\t\t\t\t\t\t\t<p class='sujet'><span class='titre'>SUJET  </span></p>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t\t<div class='col-md-9'>\n";

			$title = $line['title'];
			if(strlen($title) > 60){
				$title = substr($title, 0, 60) . "...";
			}
			echo "\t\t\t\t\t\t\t".$title."\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t</div>\n";
			echo "\t\t\t\t</div>\n";
			echo "\t\t\t\t<div class='row'>\n";
			echo "\t\t\t\t\t<div class='col-md-12'>\n";
			echo "\t\t\t\t\t\t<div class='heure'>\n";
			//$date = date('H:i:s', strtotime($line_veille['date']));
			echo "\t\t\t\t\t\t\t<p>Postée le ". $line_veille['heure'] ."</p>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t</div>\n";
			echo "\t\t\t\t</div>\n";
			echo "\t\t\t</div>\n";
			echo "\t\t\t</a>\n";
			echo "\t\t</div>\n";

	}
?>
      </div>
   </div>
<?php
include ('template/footer.php') ?>
