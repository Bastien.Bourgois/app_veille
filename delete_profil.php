<?php
$title="Supprimer le profil";

include 'core/session.php';
include 'core/loged.php';
include 'core/database.php';

if(!$id) {
  header('Location:login.php');
}
?>

<?php
include 'template/head.php';
?>

<style media="screen">
body {
  background-image: url(img/tree1.jpg);
  background-position: left;
  background-repeat: no-repeat;
  background-attachment: fixed;
}
a,
a:hover,
input {
  color:black;
  text-decoration:none;
}
</style>

<?php include 'template/header.php'; ?>

<div class="container margintop">
  <div class="row">

    <?php
    if(isset($_GET['id'])) {
      if($_GET['id']==$id) {
        if(isset($_POST['submit'])) {
          $handle=mysqli_connect("localhost","root","284569","veilleApp") or die('Erreur de connexion a la base de données');
          $query="DELETE FROM users WHERE id='$id'";
          $sql="DELETE FROM veille WHERE id_user='$id'";
          $sql1="DELETE FROM chat WHERE username='$username'";
          $sql2="DELETE FROM comment WHERE id_user='$id'";
          $sql3="DELETE FROM popularity WHERE id_user='$id'";

          $result=mysqli_query($handle,$query);
          $req=mysqli_query($handle,$sql);
          $req1=mysqli_query($handle,$sql1);
          $req2=mysqli_query($handle,$sql2);
          $req3=mysqli_query($handle,$sql3);
          header('Location:./core/logout.php');
        } else {
          echo "<form action='delete_profil.php?id=".$_GET['id']."' method=post>";
          echo "<p class='col-xs-6 col-xs-offset-2'>Etes vous sur de vouloir supprimer votre compte ainsi que tout ce qu'il contient ?</p>";
          echo "<input class='col-xs-1' name='submit' type=submit value='confirmer'>";
          echo "</form>";
          echo "<a href='membre.php' class='col-xs-1'><input type=submit value='Not today'></a>";
        }
      } else {
        header('Location:index.php');
      }
    } else {
      header('Location:index.php');
    }
    ?>

  </div>
</div>

<?php include ('template/footer.php'); ?>
