-- phpMyAdmin SQL Dump
-- version 4.6.3deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 31, 2016 at 06:59 AM
-- Server version: 5.6.30-1
-- PHP Version: 5.6.22-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `veilleApp`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `message` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_veille` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `id_user`, `id_veille`, `content`, `date`) VALUES
(4, 2, 3, 'Ta veille pue du cul', '2016-08-14 17:22:00'),
(5, 2, 3, 'Non mais vraiment', '2016-08-14 17:36:00'),
(6, 2, 3, '1/20', '2016-08-14 17:38:00'),
(7, 2, 3, '!!!', '2016-08-14 17:49:00');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `id_veille` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `valeur` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `id_veille`, `nom`, `type`, `valeur`) VALUES
(2, 52, 'phpcEPf6X.jpg', 'jpg', ''),
(3, 53, 'phpN71IsF.txt', 'txt', ''),
(4, 54, 'phpp75ibD.txt', 'txt', ''),
(5, 55, 'phpra3Ymo.odt', 'odt', ''),
(6, 56, 'php8NzPX1.jpg', 'jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `popularity`
--

CREATE TABLE `popularity` (
  `id_user` int(11) NOT NULL,
  `id_veille` int(11) NOT NULL,
  `vote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `popularity`
--

INSERT INTO `popularity` (`id_user`, `id_veille`, `vote`) VALUES
(1, 1, 1),
(3, 1, 1),
(2, 3, 1),
(0, 3, 1),
(2, 48, 1),
(2, 51, 1),
(2, 18, 1),
(2, 20, 1),
(4, 3, 1),
(2, 53, 1);

-- --------------------------------------------------------

--
-- Table structure for table `promos`
--

CREATE TABLE `promos` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `typectn`
--

CREATE TABLE `typectn` (
  `id` int(11) NOT NULL,
  `id_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `promo` varchar(50) NOT NULL,
  `img` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `firstname`, `username`, `password`, `promo`, `img`) VALUES
(1, 'Delory', 'AmÃ©lie', 'AD', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(2, 'Riem', 'Leo', 'Leos', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'phpbX8yJ8.jpg'),
(3, 'Voeux', 'Mathilde', 'Math', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(6, 'Cliquet', 'AurÃ©lie', 'ohdearzigzag', 'ca55c4c7a3e21543d37807eff30213bfacef6a8f', 'Alan Turing', 'default-thumbnail.png');

-- --------------------------------------------------------

--
-- Table structure for table `veille`
--

CREATE TABLE `veille` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `subject` varchar(255) NOT NULL,
  `keyword` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `veille`
--

INSERT INTO `veille` (`id`, `id_user`, `date`, `subject`, `keyword`, `title`, `content`) VALUES
(1, 2, '2016-06-06 00:00:00', 'VR', 'Techno', 'Hololens', 'lunettes VR/VA Microsoft'),
(2, 4, '2016-06-12 00:00:00', 'Moteur de recherche', 'Web', 'Qwant', 'Moteur de recherche francais'),
(3, 1, '2016-08-05 00:00:00', 'Robot qui fait la cuisine', 'Robotique', 'Robot cuisto', 'Un super robot qui me fais des pates tout les soirs'),
(18, 1, '2016-08-11 00:00:00', 'non', 'non', 'non', ''),
(20, 3, '2016-08-12 10:04:00', 'lklklk', 'lklk', 'lklklk', 'IMG_4394.jpg'),
(46, 3, '2016-08-12 16:58:00', 'Test', 'Visio', '123', 'phpNQxUIf.txt'),
(49, 3, '2016-08-13 21:43:00', 'lol', 'lol', 'lol', 'phpaIe57u.png'),
(51, 1, '2016-08-14 02:14:00', 'Sujet', 'Web', 'Titre', ''),
(52, 2, '2016-08-15 14:48:00', 'Un truc intÃ©ressant', 'Robotique', 'Best veille EUW', ''),
(53, 1, '2016-08-15 20:22:00', 'Crash test nÂ°35', 'Autres', 'Encore un titre qui va faire un carton', ''),
(54, 2, '2016-08-15 21:21:00', '123', 'Web', '123', ''),
(55, 2, '2016-08-15 21:25:00', '456', 'Web', '456', ''),
(56, 19, '2016-08-16 17:00:00', 'tg', 'Hacks', 'tg', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promos`
--
ALTER TABLE `promos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typectn`
--
ALTER TABLE `typectn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `veille`
--
ALTER TABLE `veille`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `promos`
--
ALTER TABLE `promos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `typectn`
--
ALTER TABLE `typectn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `veille`
--
ALTER TABLE `veille`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
