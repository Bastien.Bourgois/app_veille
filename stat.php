<?php
include ('core/session.php');
include ('core/database.php');
include ('core/permission.php');
include ('core/loged.php');
include ('include/stat_category.php');
?>
<!doctype html>
<html>
<head>
  <title>Statistiques</title>
  <meta charset>
  <?php
    include ('include/style.php')
   ?>
   <style>
    form {
      padding:20px 0 0 0;
      margin:0;
    }
    .cancel {
      padding:0;
    }
   </style>
   <script type="text/javascript">
window.onload = function () {
  var chart = new CanvasJS.Chart("chartContainer", {
    title:{
      text: "Nombre de veilles par categories"
    },

    data: [  //array of dataSeries

   { //dataSeries - second quarter

    type: "column",
    name: "Second Quarter",
    dataPoints: [
    { label: "Web", y: <?php echo $resultat_web[0] ; ?> },
    { label: "IOT", y: <?php echo $resultat_iot[0] ; ?> },
    { label: "Logiciel", y:  <?php echo $resultat_logiciel[0] ; ?> },
    { label: "Hack", y: <?php echo $resultat_hack[0] ; ?> },
    { label: "Robotique", y: <?php echo $resultat_robotique[0] ; ?>  }
    ]
  }
  ]
});

  chart.render();
}
</script>
<script type="text/javascript" src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>
</head>
<body>
<?php include('template/header.php'); ?>
<div class="container margintop">
   <div class="chat">
      <div class="chat_title">
         <h3>Espace Statistiques</h3>
      </div>
<div id="chartContainer" style="height: 300px; width: 100%;">
  </div>
</body>
<?php include ('template/footer.php'); ?>
</html>
