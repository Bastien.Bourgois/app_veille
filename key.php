<?php
include ('core/session.php');
include ('core/database.php');
include ('core/loged.php');
 ?>
<!doctype html>
<html>
<head>
  <title>Catégories</title>
    <meta charset="utf-8">
    <?php
    include ('include/style.php')
    ?>
    <script src="/template/script/random.js" type="text/javascript"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
</head>
<body>
  <?php include('template/header.php'); ?>
  <div class="container-fluid margintop">
    <div class="col-xs-8 col-xs-offset-2">
    <div class="row">
      <h3>Catégories  </h3>
</div>
      
    </div>
  </div>
</div>
    <div class="row ">
      <div class=" col-xs-8 col-xs-offset-2">
        <?php
        if(isset($_POST['submit'])) {
          $keyword=$_POST['keyword'];
          $query = "SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee FROM veille WHERE keyword='$keyword'";
          $result = mysqli_query($handle,$query);
        } elseif(isset($_POST['cancel'])) {
          header('Location:key.php');
        } else {
          $query = "SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee FROM veille";
          $result = mysqli_query($handle,$query);
        }
        if($handle->affected_rows > 0) {
          while($line=mysqli_fetch_array($result)) {
            $i--;

            echo "\t\t<div id='veille_membre'>\n";
            echo "\t\t\t<div class='row'>\n";
            echo "\t\t\t\t<div class='col-xs-2'>\n";
            echo "\t\t\t\t\t<p class='dark'>" .$line['date_formatee']."</p>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t\t<div class='col-xs-7'>\n";
            echo "\t\t\t\t\t<div class='subject'>\n";
            $title = $line['subject'];
            if(strlen($title) > 60){
              $title = substr($title, 0, 60) ."...";
            }
            echo "\t\t\t\t\t\t<p class='titre'> <a href='veille.php?id=".$line['id']."'><img class='sujet_img' src='img/sujet.png'>   " . $title." </a></p>\n";
            echo "\t\t\t\t\t\t<p> <img class='key_img' src='img/key.png'> ".$line['keyword']."</p>\n";
            echo "\t\t\t\t\t</div>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t\t<div class='col-xs-2 col-xs-offset-1'>\n";
            echo "\t\t\t\t\t<a href='veille.php?id=".$line['id']."'><img class='go' src='img/go.png'></a>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t</div>\n";
            echo "\t\t</div>\n";
          }
        } else {
          echo "\t\t\t<p>Aucune veille n'a été postée pour le moment...</p>\n";
        }
        ?>
      </div>
    </div>
  </div>
<?php include ('template/footer.php'); ?>
