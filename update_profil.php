<?php
include ('core/session.php');
include ('core/database.php');
include ('core/loged.php');
include ('core/permission.php');
 ?>
<!doctype html>
<html>
<head>
	<title>Profil</title>
	<meta charset>
  <?php
    include ('include/style.php')
   ?>
	<style>
	.mdp_warning {
	   color:grey;
	   font-weight:100;
	}
	</style>
</head>
<body>

<?php include('template/header.php');


$query = "SELECT * FROM users WHERE id='$id'";
$result = mysqli_query($handle,$query);

if($handle->affected_rows > 0) {
   $line=mysqli_fetch_array($result);
   $name=$line["name"];
   $f_name=$line["firstname"];
   $promo=$line["promo"];
   $id=$line["id"];
   } else {
      header('Location:login.php');
   }
   function promo_1($promo) {

      if($promo=="Ada Lovelace"){
         return "selected";
      }
   }
   function promo_2($promo) {
      if($promo=="Alan Turing"){
         return "selected";
      }
   }

?>
<div class="container margintop">
<?php

if(isset($_POST["submit"])){

   $username=strip_tags($_POST["username"]);
   $pass=$_POST["password"];
   $name=strip_tags($_POST["name"]);
   $f_name=strip_tags($_POST["f_name"]);
   $promo=strip_tags($_POST["promo"]);
   $repeatpassword=$_POST["repeatpassword"];


   if($username&&$name&&$f_name&&$promo) {
      if ($pass==$repeatpassword){
         $password=sha1($pass);
         if (empty($_POST["password"]) && empty($_POST["repeatpassword"])){
            $query="SELECT * FROM users WHERE id='$id'";
            $result=mysqli_query($handle,$query);
            $line=mysqli_fetch_array($result);
            $password=$line['password'];
         }
         if (isset($_FILES['img']) && $_FILES['img']['error'] == 0) {
            if ($_FILES['img']['size'] <= 5000000) {
               $info= pathinfo($_FILES['img']['name']);
               $extension_info=$info['extension'];
               $extensions=array('jpg', 'jpeg', 'png');
               if (in_array($extension_info, $extensions)) {
                  $img=basename($_FILES['img']['tmp_name']).".".$extension_info;
                  move_uploaded_file($_FILES['img']['tmp_name'], '../uploads/'.$img);
                  if ($user==$username) {
                     $query="UPDATE `users` SET `name`=('$name'), `firstname`=('$f_name'), `username`=('$username'), `password`=('$password'), `promo`=('$promo'), `img`=('$img') WHERE `id`=('$id')";
                     $result = mysqli_query($handle,$query);

                     die("Modification appliqué <a href='membre.php'>Votre profil</a>");
                  } else {
                     $query="SELECT * FROM users WHERE username= '$username'";
                     $result = mysqli_query($handle,$query);
                     if($result->num_rows > 0) {
                        echo "Ce pseudo n'est pas disponible";
                     } else {
                        $query="UPDATE `users` SET `name`=('$name'), `firstname`=('$f_name'), `username`=('$username'), `password`=('$password'), `promo`=('$promo'), `img`=('$img') WHERE `id`=('$id')";
                        $result = mysqli_query($handle,$query);
                        $_SESSION['username']=$username;
                        die("Modifications appliquées <a href='membre.php'>Votre profil</a>");
                        }
                     }
               } else {
                  echo "<p class='error'>* Format de fichier non pris en compte</p>";
                  }
            } else {
               echo "<p class='error'>* La taille du fichier est trop importante</p>";
               }
         } else {
         $img="default-thumbnail.png";
         if ($user==$username) {
                        $query="UPDATE `users` SET `name`=('$name'), `firstname`=('$f_name'), `username`=('$username'), `password`=('$password'), `promo`=('$promo'), `img`=('$img') WHERE `id`=('$id')";
                        $result = mysqli_query($handle,$query);
                        die("Modification appliqué <a href='membre.php'>Votre profil</a>");
                     } else {
                        $query="SELECT * FROM users WHERE username= '$username'";
                        $result = mysqli_query($handle,$query);
                        if($result->num_rows > 0) {
                           echo "Ce pseudo n'est pas disponible";
                        } else {
                           $query="UPDATE `users` SET `name`=('$name'), `firstname`=('$f_name'), `username`=('$username'), `password`=('$password'), `promo`=('$promo'), `img`=('$img') WHERE `id`=('$id')";
                           $result = mysqli_query($handle,$query);
                           $_SESSION['username']=$username;
                           die("Modification appliqué <a href='membre.php'>Votre profil</a>");
                           }
                        }
            }
      } else {
         echo "<p class='error'>* Les deux mots de passe doivent être identiques</p>";
         }
   } else {
   echo "<p class='error'>* Veuillez saisir tous les champs</p>";
      }
}

?>

	<form method="POST" action="update_profil.php" enctype="multipart/form-data">
		<div class="form-group">
			<label for="username">Pseudo</label>
			<input type="text" class="form-control" name="username" placeholder="Votre identifiant de connexion" value="<?php echo $username; ?>">
		</div>
		<div class="form-group">
			<label for="name">Nom</label>
			<input type="text" class="form-control" name="name" placeholder="Votre nom" value="<?php echo $name; ?>">
		</div>
	  <div class="form-group">
	    <label for="f_name">Prénom</label>
	    <input type="text" class="form-control" name="f_name" placeholder="Votre prénom" value="<?php echo $f_name; ?>">
	  </div>
		<div class="form-group">
		<label>Promotion</label>
		<select name="promo" class="form-control">
			<option value="Ada Lovelace" <?php echo promo_1($promo); ?>>Ada Lovelace</option>
			<option value="Alan Turing" <?php echo promo_2($promo); ?>>Alan Turing</option>
		</select>
	</div>
	<div class="form-group">
         <label for="img">Photo :</label>
         <input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
         <input class="champ" tabindex="7" type="file" name="img"><br>
   </div>
	<div class="form-group">
	    <p class="mdp_warning"><i>Laissez les champs ci-dessous vide si vous ne souhaitez pas modifier votre mot de passe</i></p>
	    <label for="password">Mot de passe</label>
	    <input type="password" class="form-control" name="password" placeholder="Mot de passe">
	  </div>
		<div class="form-group">
			<label for="repeatpassword">Confirmer le mot de passe</label>
			<input type="password" class="form-control" name="repeatpassword" placeholder="Confirmer le mot de passe">
		</div>
	  <button type="submit" name ="submit" class="btn btn-info">Enregistrer les modifications</button>
	</form>
</div>

<?php include ('template/footer.php'); ?>
