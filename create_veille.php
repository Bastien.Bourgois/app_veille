<?php
$title="Poster une veille";

include ('core/session.php');
include ('core/database.php');
include ('core/permission.php');
include ('core/loged.php');
?>

<?php
include ('template/head.php');
include ('template/header.php');
?>

<div class="container margintop">
	<div class="row">
		<div class="col-md-12">
			<div id="bienvenue">

				<?php
				include 'include/date_insert.php';

				//Declare les variable a base des POST
				$subject=strip_tags($_POST["subject"]);
				$title=strip_tags($_POST["title"]);
				$keyword=strip_tags($_POST["keyword"]);
				$date=strip_tags($_POST["date"]);
				$id_user=strip_tags($_POST["id_user"]);
				$description=strip_tags($_POST["description"]);

				include 'include/file.php';
				?>

				<h3>Ajouter ma veille</h3>
				<form action="create_veille.php" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<label for="subject">Sujet</label>
						<input class="champ form-control" tabindex="1" type="text" name="subject" placeholder="Subject..."><br>
					</div>
					<div class="form-group">
						<label for="title">Titre</label>
						<input class="champ form-control" tabindex="2" type="text" name="title" placeholder="Title..."><br>
					</div>
					<div class="form-group">
						<label>Catégories</label>
						<select name="keyword" tabindex="3" class="form-control">
							<option value="Web">Web</option>
							<option value="Objets connecté">Objets connectés</option>
							<option value="Logiciel">Logiciel</option>
							<option value="Hacks">Hack</option>
							<option value="Robotique">Robotique</option>
							<option value="Autres">Autre</option>
						</select>
					</div>
					<div class="form-group">
						<input  type="hidden" name="date" value="<?php echo $year."/".$mois."/".$jour."/".$heure."/".$minute; ?>">
						<label for="contenu">Contenu</label>
						<input class="champ" tabindex="4" type="file" name="content"><br>
						<label for="contenu">Description / lien URL</label>
						<textarea type='text' class='form-control' name="description" ></textarea>
						<input type="hidden" name="id_user" value="<?php echo $id; ?>">
						<input type="submit" tabindex="5" name="submit" class="btn btn-info" value="Ajouter">
					</form>
				</div>
			</div>

			<?php include ('template/footer.php'); ?>
