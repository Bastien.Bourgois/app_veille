<head>
  <meta charset="utf-8">
  <?php echo "<title>" . $title . "</title>"; ?>
  <script src="/template/script/random.js" type="text/javascript"></script>

  <?php
    include ('include/style.php')
   ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<?php
$title="Chat";

include ('core/session.php');
include ('core/database.php');
include ('core/loged.php');

$query="SELECT * FROM chat";
$result=mysqli_query($handle,$query);
$line=mysqli_fetch_array($result);
?>


<?php include('template/header.php'); ?>

<div class="container margintop">
  <div class="chat">
    <div class="chat_title">
      <h3>Espace Chat</h3>
    </div>

    <?php
    // Connexion à la base de données
    if (isset($_POST['submit'])) {

      $query="INSERT INTO chat (username,message) VALUES ('$username',\"" . $_POST["message"] . "\")";
      $result=mysqli_query($handle,$query);

      // Redirection du visiteur vers la page du minichat
      header('Location: chat.php');
    }

    // Récupération des 10 derniers messages
    $query="SELECT * FROM chat ORDER BY id DESC LIMIT 0, 100";
    // Affichage de chaque message (toutes les données sont protégées par htmlspecialchars)
    $result=mysqli_query($handle,$query);
    ?>

    <div class="zone">

      <?php
      echo "\t\t\t<ul>\n";
      while($line=mysqli_fetch_array($result)) {
        echo "\t\t\t\t<li>\n";
        echo "\t\t\t\t\t<strong>".$line["username"]." : ". "</strong>\n";
        echo "\t\t\t\t\t".$line["message"]."\n";
        echo "\t\t\t\t</li>\n";
      }
      echo "\t\t\t</ul>\n";
      ?>

    </div>
  </div>
  <div class="message">
    <form action="chat.php" method="post">
      <label for="message">Message</label> :  <input type="text" name="message" id="message" />
      <input type="submit" class="btn btn-success" name="submit" value="ENVOYER" />
    </form>
  </div>
</div>

<?php include ('template/footer.php'); ?>
