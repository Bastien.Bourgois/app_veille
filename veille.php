<?php
include ('core/session.php');
include ('core/database.php');
include ('core/permission.php');
include ('core/loged.php');
 ?>
<!doctype html>
<html>
<head>
	<title>Veille</title>
	<meta charset>
  <?php
    include ('include/style.php')
   ?>
	<style media="screen">

	</style>
</head>
<body>
<?php
	include('template/header.php');
?>

	<div class="container margintop">
		<div class='col-md-8 col-md-offset-2'>
			<div class='row'>
				<div class='col-md-12'>
					<?php
					if (isset($_GET['id'])) {
						//Les requêtes
						$id_veille=$_GET['id'];
						$query="SELECT *, DATE_FORMAT(date, '%d/%m à %H:%i') as date_formatee FROM veille WHERE id='$id_veille'";
						$line=mysqli_fetch_array(mysqli_query($handle,$query));
						if($line["id_user"] != $id) {
							$permitted=0;
						}
						elseif ($line["id_user"]==$id) {
							$permitted=1;
						}
						$user=$line['id_user'];
						$sql="SELECT * FROM users WHERE id='$user'";
						$user=mysqli_fetch_array(mysqli_query($handle,$sql));
						$sql2="SELECT * FROM popularity WHERE id_veille='$id_veille'";
						$nbv=mysqli_query($handle,$sql2)->num_rows;
						$sqli="SELECT *, DATE_FORMAT(date, '%d/%m à %H:%i') as date_format FROM comment WHERE id_veille='$id_veille' ORDER BY date DESC";
						$result=mysqli_query($handle,$sqli);
						//Valeurs simplifié en variables
						$img=$user['img'];
						$title=$line['title'];
						$subject=$line['subject'];
						$date=$line['date_formatee'];
						$keyword=$line['keyword'];
						$firstname=$user['firstname'];
						$name=$user['name'];
						$comment=$req['content'];
						echo "<h3>".$title. "</h3>";
						echo " <p class='nom'> <img class='personne' src='../uploads/".$img."'>";
						echo $firstname." ".$name. "</p>";
						echo "</div>";
						echo "</div>";
						echo "<div class='contenu'>";
						echo "<div class='row'>";
						echo "<div class='col-md-6 '>";
						echo "<p class='sujet'><img class='sujet_img' src='img/sujet2.png'><span class='titre'>Sujet :</span> ".$subject."</p>";
						echo "<br>";
						echo "<p class='sujet'><img class='key_img' src='img/key2.png'><span class='titre'>Catégorie : </span>".$keyword."</p>";
						echo "<br>";
						$query3="SELECT * FROM contents WHERE id_veille='$id_veille'";
						$result3=mysqli_fetch_array(mysqli_query($handle,$query3));
						$type=$result3['type'];
						$ndf=$result3['nom'];

						echo "<a href='../uploads/".$ndf."'>";
						echo "<img src='img/".$type.".png'>  ";
						echo $ndf;
						echo "</a>";
						echo "</div>";
						echo "<div class='col-md-6'>";
						echo "<p ><img src='img/time.png'> " .$date."</p>";
						echo "<div>";
						echo "</div>";
						echo "</div>";
						echo "</div>";
						echo "</div>";
						echo "<div class='row'>";
						echo "<div class='col-md-12'>";
						echo "<img src='img/describe.png'>";
						echo $result3['description'];
						echo "<br><br>";
						if($permitted==1) {
							echo "<a href='delete_veille.php?id=".$line['id']."'><img class='delete' src='img/delete.png'> Supprimer </a>";
							echo "<a href='update_veille.php?id=".$line['id']."'>  <img class='update' src='img/update.png'> Modifier </a>";
							echo "<br><br>";
						} elseif($permitted==0) {
							echo "<span class='nom'>Commentaires</span>";
						}
						if($logged == 1){
							if ($line['id_user']!=$id) {

								echo "<a href='like.php?id=".$line['id']."'>Ajouter un vote... </a>";

							}

							echo "<img src='img/heart.png'> ...  <span class='vote'>" .$nbv."</span>";
							echo "<form action='comment.php?id=".$id_veille."' method='post'>";
							echo "<label for='comment'> Laisser un commentaire </label>";
							echo "<textarea class='form-control' name='comment'></textarea>";
							echo "<input  type='submit'>";
							echo "</form>";
							echo "</div>";
							echo "</div>";
						}
					} else {
						header('Location:index.php');
					}
					echo "<br>";
					echo "<br>";
					while($com=mysqli_fetch_array($result)) {
						$id_pub=$com['id_user'];
						$sqlu="SELECT * FROM users WHERE id='$id_pub'";
						$pub=mysqli_fetch_array(mysqli_query($handle,$sqlu));
						echo "<div class='com'>";
						echo "<div class='row'>";
						echo "<div class='col-md-2'>";
						echo "<span class='titre1'>".$pub['firstname']. " " .$pub['name'];
						echo "<span class='dark'><br>".$com['date_format']. " </span></span>";
						echo "<br>";
						echo "<br>";
						echo "</div>";
						echo "<div class='col-md-10'>";
						echo "<div class='com1'>".$com['content']."</div>";
						echo "</div>";
						echo "</div>";
						echo "</div>";

					}
					?>
				</div>
			</div>
		</div>
<?php include 'template/footer.php'; ?>
